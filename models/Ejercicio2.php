<?php
namespace app\models;

use yii\base\Model;

class Ejercicio2 extends Model{
    public $numero1;
    
    public function AttributeLabels(){
        return [
            "numero1" => "Introduce un numero"
        ];
    }
    
    public function rules(){
        return [
          ['numero1','integer','min'=>10],
          ['numero1','required'],
        ];
    }
    
}
