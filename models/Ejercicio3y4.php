<?php
namespace app\models;

use yii\base\Model;

class Ejercicio3y4 extends Model{
    public $numero1;
    public $numero2;
    public $numero3;
    
    public function AttributeLabels(){
        return [
            "numero1" => "Introduce numero 1",
            "numero2" => "Introduce numero 2",
            "numero3" => "Introduce numero 3",
        ];
    }
    
    public function rules(){
        return [
          [['numero1','numero2','numero3'],'integer','message'=>'El campo {attribute} debe ser numero'],
          [['numero1','numero2','numero3'],'required'],
          ['numero3','compare','compareAttribute' => 'numero2','operator'=>'>']
        ];
    }
    
}
