<?php
namespace app\models;

use yii\base\Model;

class Ejercicio1 extends Model{
    public $numero1;
    
    public function AttributeLabels(){
        return [
            "numero1" => "Introduce un numero"
        ];
    }
    
    public function rules(){
        return [
          ['numero1','integer','min'=>1,'max'=>100],
          ['numero1','required'],
        ];
    }
    
}
