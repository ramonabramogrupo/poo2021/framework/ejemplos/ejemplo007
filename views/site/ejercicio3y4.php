<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ejercicio3y4 */
/* @var $form ActiveForm */
?>
<div class="site-ejercicio3y4">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'numero1') ?>
        <?= $form->field($model, 'numero2') ?>
        <?= $form->field($model, 'numero3') ?>
    
        <div class="form-group">
            <?= Html::submitButton($operacion, ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio3y4 -->
