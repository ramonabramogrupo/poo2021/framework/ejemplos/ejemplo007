<?php

/* @var $this yii\web\View */

$this->title = 'Operaciones';
?>
<div class="site-index">
   
    <div class="body-content">

        <div class="row">
            <div class="col-lg-8 mx-auto bg-light p-4 rounded">
                <h2>Operaciones con formularios</h2>

                <p>Creando formularios utilizando modelos de Yii</p>
            </div>
        </div>

    </div>
</div>
